# yii2-module/yii2-information

A module that processes atomic information units.

![coverage](https://gitlab.com/yii2-module/yii2-information/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-information/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-information ^8`


## Basic Usage

This module needs the following components to be set at the configuration level:

- 'db_information' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_information' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.


This module needs the following parameters to be set at the configuration level:

- (none)


You should also modify the configuration of the yii application with the 
following changes :

```php
[
	...
	'modules' => [
		...
		'information' => [
			'class' => 'Yii2Module\Yii2Information\InformationModule',
		],
		...
	],
	...
]
```


## Console API

The console API uses the following commands, assuming your working directory
is the directory where the yii (console) file is, and that the configuration
to this module has been named `information`.


`./yii information/process/delete-for-module --moduleId={{moduleId}}`

This method deletes all informations related to the module with the given id in
the informations tables.


`./yii information/process/module --moduleId={{moduleId}}`

This method resolves all the records that are stored in the information tables
for the module with the given id.


`./yii information/process/data --moduleId={{moduleId}}`

This method resolves only the information data that are stored for the module
with the given id.


`./yii information/process/relation --moduleId={{moduleId}}`

This method resolves only the information relation that are stored for the
module with the given id.


## PHP API

The php API, available in the `InformationModule` component, is defined as
follow :


```php

use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use Psr\Log\LoggerInterface;

getStorageHandler(Module $module, LoggerInterface $logger, boolean $checkBeforeInsert = false) : InformationVisitorInterface
```

This method gets an information handler to handle all the informations that
could be thrown at it. If the `$checkBeforeInsert` parameter is set to
true, then this method will decorate the handler with another handler which 
checks in the current database if the information exists, and if it exists, it
will skip storing it. That handler will not check for accuracy of the information,
only its existence. This handler is more useful in a multi threaded environment
due to the fact it maximizes parallelization of resolving records.


```php

use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use Psr\Log\LoggerInterface;

getResolverHandler(Module $module, LoggerInterface $logger) : InformationVisitorInterface
```

This method gets an information handler to handle all the informations that
could be thrown at it. It will try to resolve it like all the other `process`
methods below, and if it cannot, it will store the record like the storage
handler up there. This handler is more useful in a single threaded environment
due to the fact it minimizes the number of queries to the database.


```php
processForModule(Module $module, LoggerInterface $logger = null) : void
```

This method processes all kinds of information for given module. In this context,
processing means resolving individual information context to be able to store
all of those informations within a relational database which is tailored to
handle them. All informations that couldn't be stored accordingly stay in the
informations database.


```php
deleteForModule(Module $module) : void
```

This method deletes all informations related to the given module in the 
informations tables.


```php
processInformationData(Module $module, LoggerInterface $logger = null) : void
```

This method processes all the informations of type data for given module. Such
informations are assuming the receiving database holds a `path` field in order
to store the information's primary key or identifier.


```php
processInformationRelation(Module $module, LoggerInterface $logger = null) : void
```

This method processes all the informations of type relation for given module.
Such informations are assuming the receiving database holds a `path` field in
order to store the information's primary key or identifier.


## License

MIT. See [license file](LICENSE "license file").
