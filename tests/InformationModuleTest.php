<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Module\Yii2Information\InformationModule;

/**
 * InformationModuleTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Module\Yii2Information\InformationModule
 *
 * @internal
 *
 * @small
 */
class InformationModuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationModule
	 */
	protected InformationModule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationModule('information');
	}
	
}
