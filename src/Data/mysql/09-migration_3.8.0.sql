/**
 * Database migration for by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

DROP TABLE IF EXISTS `information_data`;
DROP TABLE IF EXISTS `information_relation`;
