/**
 * Database structure required by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `information_triple`;
DROP TABLE IF EXISTS `information_object`;

SET foreign_key_checks = 1;


CREATE TABLE `information_triple`
(
	`information_triple_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The id of the information triple',
	`meta_created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date of registration of this object',
	`module_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The module that registered this object',
	`info_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The identifier of this information',
	`subject` VARCHAR(700) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The subject of the triple (object)',
	`predicate` VARCHAR(700) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The predicate of the triple (relation)',
	`object` TEXT NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The object of the triplet (data)',
	INDEX `information_triple_module` (`module_id`, `information_triple_id`)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci
COMMENT='The table to store the basic information data';


CREATE TABLE `information_object`
(
	`information_object_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The id of the information relation',
	`meta_created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date of registration of this object',
	`module_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The module that registered this object',
	`info_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The identifier of this information',
	`object_class` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The class of the support object',
	`object_keys` TEXT NOT NULL COLLATE ascii_bin COMMENT 'The primary keys in that field of support object',
	`object_rels` TEXT NOT NULL DEFAULT '[]' COLLATE ascii_bin COMMENT 'The relations in that field of support object',
	`object_data` MEDIUMTEXT NOT NULL DEFAULT '[]' COLLATE ascii_bin COMMENT 'The data in that field of support object',
	INDEX `information_object_module` (`module_id`, `information_object_id`)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci
COMMENT='The table to store the all object informations';

