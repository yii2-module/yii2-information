/**
 * Database migration for by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

ALTER TABLE `information_triple` 
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_data`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_data_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_relation`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_relation_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_relation_data`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_relation_data_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_composite_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_composite_data`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_composite_data_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_composite_relation`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;

ALTER TABLE `information_composite_relation_timed`
ADD `origin_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The identifier of the origin for this information' AFTER `module_id`,
ADD `info_id` VARCHAR(128) NOT NULL COLLATE latin1_general_ci COMMENT 'The idetifier of this information' AFTER `origin_id`,
ADD `info_nb` INT(11) NOT NULL COMMENT 'The number of the information when generated' AFTER `info_id`;
