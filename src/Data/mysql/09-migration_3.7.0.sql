/**
 * Database migration for by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `origin_id`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `origin_id`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `origin_id`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `origin_id`;

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `info_nb`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `info_nb`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `info_nb`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `info_nb`;
