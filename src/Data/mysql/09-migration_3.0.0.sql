/**
 * Database migration for by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

DROP TABLE IF EXISTS `information_data_timed`;
DROP TABLE IF EXISTS `information_relation_timed`;
DROP TABLE IF EXISTS `information_relation_data`;
DROP TABLE IF EXISTS `information_relation_data_timed`;
DROP TABLE IF EXISTS `information_composite_timed`;
DROP TABLE IF EXISTS `information_composite_data`;
DROP TABLE IF EXISTS `information_composite_data_timed`;
DROP TABLE IF EXISTS `information_composite_relation`;
DROP TABLE IF EXISTS `information_composite_relation_timed`;

DROP VIEW IF EXISTS `v_information_data_timed`;
DROP VIEW IF EXISTS `v_information_relation_timed`;
DROP VIEW IF EXISTS `v_information_relation_data`;
DROP VIEW IF EXISTS `v_information_relation_data_timed`;
DROP VIEW IF EXISTS `v_information_composite_timed`;
DROP VIEW IF EXISTS `v_information_composite_data`;
DROP VIEW IF EXISTS `v_information_composite_data_timed`;
DROP VIEW IF EXISTS `v_information_composite_relation`;
DROP VIEW IF EXISTS `v_information_composite_relation_timed`;
