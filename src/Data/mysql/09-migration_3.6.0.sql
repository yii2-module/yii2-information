/**
 * Database migration for by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `category`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `category`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `category`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `category`;

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `thrd_process_id`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `thrd_process_id`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `thrd_process_id`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `thrd_process_id`;

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `thrd_status_code`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `thrd_status_code`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `thrd_status_code`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `thrd_status_code`;

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `thrd_nb_tries`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `thrd_nb_tries`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `thrd_nb_tries`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `thrd_nb_tries`;

ALTER TABLE `information_triple` DROP COLUMN IF EXISTS `thrd_last_try`;
ALTER TABLE `information_data` DROP COLUMN IF EXISTS `thrd_last_try`;
ALTER TABLE `information_relation` DROP COLUMN IF EXISTS `thrd_last_try`;
ALTER TABLE `information_object` DROP COLUMN IF EXISTS `thrd_last_try`;
