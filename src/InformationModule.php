<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information;

use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use Psr\Log\LoggerInterface;
use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2Information\Components\InformationBatchProcessor;
use Yii2Module\Yii2Information\Components\InformationDeletorProcessor;
use Yii2Module\Yii2Information\Components\InformationFallbackHandler;
use Yii2Module\Yii2Information\Components\InformationHashChecker;
use Yii2Module\Yii2Information\Components\InformationHashSetter;
use Yii2Module\Yii2Information\Components\InformationPathChecker;
use Yii2Module\Yii2Information\Components\InformationPathSetter;
use Yii2Module\Yii2Information\Components\InformationRawSetter;
use Yii2Module\Yii2Information\Components\InformationRecordHandler;
use Yii2Module\Yii2Information\Models\InformationObject;
use Yii2Module\Yii2Information\Models\InformationTriple;

/**
 * InformationModule class file.
 *
 * This module is made to have a store for informations that are gathered by
 * other modules. The role of this module is to store them and get them back
 * for processing, as well as managing multiple processus that are processing
 * all those data.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InformationModule extends BootstrappedModule
{
	
	
	// ---- ---- ---- ---- InformationModule specific API ---- ---- ---- ---- \\
	
	/**
	 * Gets an information handler suited for given module. This handler will
	 * store all the information within the tables on this module to be
	 * processed later.
	 *
	 * This handler is useful in a multi threaded environment when multiple
	 * threads empties the file of records in the information module tables.
	 *
	 * @param Module $module
	 * @param ?LoggerInterface $logger
	 * @param boolean $checkBeforeInsert if true, the module will check for the
	 *                                   existence of the given information. If it already exists in the
	 *                                   model, it will ignore it (and register it otherwise).
	 *                                   If false, this will register the information without question.
	 * @param boolean $useOldPath true if use 'path', false if 'hash' strategy
	 * @return InformationVisitorInterface<boolean>
	 */
	public function getStorageHandler(Module $module, ?LoggerInterface $logger = null, bool $checkBeforeInsert = false, bool $useOldPath = false) : InformationVisitorInterface
	{
		$handler = new InformationRecordHandler($module);
		if(null !== $logger)
		{
			$handler = new LoggerInformationVisitor($logger, $handler);
		}
		
		if(!$checkBeforeInsert)
		{
			return $handler;
		}
		
		$pathChecker = $useOldPath ? new InformationPathChecker() : new InformationHashChecker();
		if(null !== $logger)
		{
			$pathChecker = new LoggerInformationVisitor($logger, $pathChecker);
		}
		
		return new InformationFallbackHandler($pathChecker, $handler);
	}
	
	/**
	 * Gets an information handler for the given module that uses a specific
	 * field named 'path' for primary key handling. This handler witll try to
	 * resolve all the information with the records to the real tables and if
	 * it cannot, will store the infomration to the information module tables.
	 * 
	 * @param Module $module
	 * @param ?LoggerInterface $logger
	 * @return InformationVisitorInterface<boolean>
	 */
	public function getPathResolverHandler(Module $module, ?LoggerInterface $logger = null) : InformationVisitorInterface
	{
		$resolver = $this->decorateLoggerHandler(new InformationPathSetter(), $logger);
		
		return new InformationFallbackHandler($resolver, new InformationRecordHandler($module));
	}
	
	/**
	 * Gets an information handler for the given module that uses hashed data
	 * for primary key handling. This handler will try to resolve all the
	 * information with the records to the real tables, and if it cannot, will
	 * store the information to the information module tables.
	 * 
	 * @param Module $module
	 * @param ?LoggerInterface $logger
	 * @return InformationVisitorInterface<boolean>
	 */
	public function getHashResolverHandler(Module $module, ?LoggerInterface $logger = null) : InformationVisitorInterface
	{
		$resolver = $this->decorateLoggerHandler(new InformationHashSetter(), $logger);
		
		return new InformationFallbackHandler($resolver, new InformationRecordHandler($module));
	}
	
	/**
	 * Gets an information handler for the given module that uses raw data for
	 * primary key handling. This handler will try to resolve all the
	 * information with the records to the real tables, and if it cannot, will
	 * store the information to the information module tables.
	 * 
	 * @param Module $module
	 * @param ?LoggerInterface $logger
	 * @return InformationVisitorInterface<boolean>
	 */
	public function getRawResolverHandler(Module $module, ?LoggerInterface $logger = null) : InformationVisitorInterface
	{
		$resolver = $this->decorateLoggerHandler(new InformationRawSetter(), $logger);
		
		return new InformationFallbackHandler($resolver, new InformationRecordHandler($module));
	}
	
	/**
	 * Processes all the records for given module with given logger.
	 *
	 * @param ?Module $module
	 * @param ?LoggerInterface $logger
	 * @param bool $useOldPath true if use 'path', false if 'hash' strategy
	 */
	public function processForModule(?Module $module, ?LoggerInterface $logger = null, bool $useOldPath = false) : void
	{
		if(null === $module)
		{
			return;
		}
		
		$resolver = $useOldPath ? new InformationPathSetter() : new InformationHashSetter();
		if(null !== $logger)
		{
			$resolver = new LoggerInformationVisitor($logger, $resolver);
		}
		$processor = new InformationBatchProcessor($resolver);
		$processor->processForModule($module);
	}
	
	/**
	 * Processes to deletes all the records in all tables for given module.
	 *
	 * @param ?Module $module
	 */
	public function deleteForModule(?Module $module) : void
	{
		if(null === $module)
		{
			return;
		}
		
		$processor = new InformationDeletorProcessor();
		$processor->processForModule($module);
	}
	
	/**
	 * Processes all the records on information object for given module with
	 * given logger.
	 *
	 * @param ?Module $module
	 * @param ?LoggerInterface $logger
	 */
	public function processInformationObject(?Module $module, ?LoggerInterface $logger = null) : void
	{
		if(null === $module)
		{
			return;
		}
		
		$resolver = new InformationPathSetter();
		if(null !== $logger)
		{
			$resolver = new LoggerInformationVisitor($logger, $resolver);
		}
		$processor = new InformationBatchProcessor($resolver);
		$processor->processInformationObject($module);
	}
	
	/**
	 * Processes all the records on information triple for given module
	 * with given logger.
	 *
	 * @param ?Module $module
	 * @param ?LoggerInterface $logger
	 */
	public function processInformationTriple(?Module $module, ?LoggerInterface $logger = null) : void
	{
		if(null === $module)
		{
			return;
		}
		
		$resolver = new InformationPathSetter();
		if(null !== $logger)
		{
			$resolver = new LoggerInformationVisitor($logger, $resolver);
		}
		$processor = new InformationBatchProcessor($resolver);
		$processor->processInformationTriple($module);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'info-circle';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InformationModule.Module', 'Information');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'information' => new Bundle(BaseYii::t('InformationModule.Module', 'Information'), [
				'object' => (new Record(InformationObject::class, 'object', BaseYii::t('InformationModule.Module', 'Object')))->enableFullAccess(),
				'triple' => (new Record(InformationTriple::class, 'triple', BaseYii::t('InformationModule.Module', 'Triple')))->enableFullAccess(),
			]),
		];
	}
	
	/**
	 * Decorates the given visitor with a logger visitor.
	 * 
	 * @param InformationVisitorInterface<boolean> $visitor
	 * @param ?LoggerInterface $logger
	 * @return InformationVisitorInterface<boolean>
	 */
	protected function decorateLoggerHandler(InformationVisitorInterface $visitor, ?LoggerInterface $logger) : InformationVisitorInterface
	{
		if(null !== $logger)
		{
			$visitor = new LoggerInformationVisitor($logger, $visitor);
		}
		
		return $visitor;
	}
	
}
