<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Commands;

use InvalidArgumentException;
use yii\base\Module;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2Information\InformationModule;

/**
 * ProcessCommand class file.
 *
 * This class provides all the commands to process all the information tables
 * that are given in this module for a given other module that may have put
 * informations in those tables.
 *
 * @author Anastaszor
 */
class ProcessController extends ExtendedController
{
	
	/**
	 * Drops all the records concerning a specific module in all the tables.
	 *
	 * @param string $moduleId the path of the module in the yii application
	 * @return integer the error code, 0 if no error
	 */
	public function actionDeleteForModule(string $moduleId) : int
	{
		return $this->runCallable(function() use ($moduleId) : int
		{
			$module = $this->getModuleFromId($moduleId);
			InformationModule::getInstance()->deleteForModule($module);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Processes all the records in all the tables for the module found by
	 * given path.
	 *
	 * @param string $moduleId the path of the module in the yii application
	 * @return integer the error code, 0 if no error
	 */
	public function actionModule(string $moduleId) : int
	{
		return $this->runCallable(function() use ($moduleId) : int
		{
			$module = $this->getModuleFromId($moduleId);
			InformationModule::getInstance()->processForModule($module, $this->getLogger());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Processes all the records in the information object table for the module
	 * found by given path.
	 *
	 * @param string $moduleId the path of the module in the yii application
	 * @return integer the error code, 0 if no error
	 */
	public function actionObject(string $moduleId) : int
	{
		return $this->runCallable(function() use ($moduleId) : int
		{
			$module = $this->getModuleFromId($moduleId);
			InformationModule::getInstance()->processInformationObject($module, $this->getLogger());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Processes all the records in the information triple table for the
	 * module found by given path.
	 *
	 * @param string $moduleId the path of the module in the yii application
	 * @return integer the error code, 0 if no error
	 */
	public function actionTriple(string $moduleId) : int
	{
		return $this->runCallable(function() use ($moduleId) : int
		{
			$module = $this->getModuleFromId($moduleId);
			InformationModule::getInstance()->processInformationTriple($module, $this->getLogger());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Gets the module for given path id.
	 *
	 * @param string $moduleId
	 * @return Module
	 * @throws InvalidArgumentException
	 */
	protected function getModuleFromId(string $moduleId) : Module
	{
		$module = BaseYii::$app->getModule($moduleId);
		if(null === $module)
		{
			$message = 'The module {moduleId} was not found in the application.';
			$context = ['{moduleId}' => $moduleId];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $module;
	}
	
}
