<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use PhpExtended\Information\InformationStateInformation;

/**
 * InformationState class file.
 * 
 * This class is a simple implementation of the InformationStateInformation.
 * 
 * @author Anastaszor
 */
class InformationState implements InformationStateInformation
{
	
	/**
	 * Whether the information is pending.
	 * 
	 * @var boolean
	 */
	protected bool $_pending;
	
	/**
	 * Whether the information is rejected.
	 * 
	 * @var boolean
	 */
	protected bool $_rejected;
	
	/**
	 * Builds a new InformationState with its inner data.
	 * 
	 * @param boolean $pending
	 * @param boolean $rejected
	 */
	public function __construct(bool $pending, bool $rejected)
	{
		$this->_pending = $pending;
		$this->_rejected = $rejected;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_pending ? 'PENDING' : ($this->_rejected ? 'REJECTED' : 'UNKNOWN');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationStateInformation::isPending()
	 */
	public function isPending() : bool
	{
		return $this->_pending;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationStateInformation::isRejected()
	 */
	public function isRejected() : bool
	{
		return $this->_rejected;
	}
	
}
