<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\Information\InformationStateInformation;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;
use Yii2Module\Yii2Information\Models\InformationTriple;

/**
 * InformationTripleAdapter class file.
 *
 * This class adapts the InformationTriple records to the
 * InformationTripleInterface interface.
 *
 * @author Anastaszor
 */
class InformationTripleAdapter implements InformationTripleInterface
{
	
	/**
	 * The InformationTriple record.
	 *
	 * @var InformationTriple
	 */
	protected InformationTriple $_record;
	
	/**
	 * Builds a new InformationTripleAdapter with the given record.
	 *
	 * @param InformationTriple $record
	 */
	public function __construct(InformationTriple $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.((string) $this->_record->information_triple_id);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getState()
	 */
	public function getState() : InformationStateInformation
	{
		return new InformationState(false, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_record->info_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getCreationDate()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getCreationDate() : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->_record->meta_created_date);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getSupportClass()
	 */
	public function getSupportClass() : string
	{
		return $this->getPredicate();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getSubject()
	 */
	public function getSubject() : string
	{
		return $this->_record->subject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getPredicate()
	 */
	public function getPredicate() : string
	{
		return $this->_record->predicate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getObject()
	 */
	public function getObject() : ?string
	{
		return $this->_record->object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::isCacheable()
	 */
	public function isCacheable() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getEtag()
	 */
	public function getEtag() : string
	{
		return \sha1($this->getSubject().'|'.((string) $this->getObject()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::beVisitedBy()
	 */
	public function beVisitedBy(InformationVisitorInterface $visitor) : bool
	{
		return (bool) $visitor->visitTriple($this);
	}
	
}
