<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;
use yii\BaseYii;
use yii\db\Exception;

/**
 * InformationHashSetter class file.
 * 
 * This class is a resolver (meaning it will register the information not in
 * the information table, but will try to resolve it to save it at the right
 * place in the target relational model) that uses the primary key field as
 * CHAR(40) to store sha1 hashes.
 * 
 * All the methods return a boolean which it true if the information was
 * successfully processed and an information was saved, and false if the
 * information could not be processed. An exception is thrown in case the
 * metadata of the information are compativery to the model state and the
 * information cannot be considered as processed and removed.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 */
class InformationHashSetter extends InformationResolver implements InformationVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		return !(empty($information->getSubject()) || empty($information->getPredicate()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitMulti()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		if(empty($information->getPrimaryKey()))
		{
			return false;
		}
		
		$model = $this->getSupportModel($information);
		
		foreach(\array_keys($information->getPrimaryKey()) as $key)
		{
			$this->checkAttribute($model, $key);
		}
		
		$object = $model::findOne($information->getSmartPrimaryKeySha1());
		if(null === $object)
		{
			$object = clone $model;
			$object->setIsNewRecord(true);
			
			foreach($information->getSmartPrimaryKeySha1() as $field => $value)
			{
				$object->setAttribute($field, $value);
			}
		}
		$needsToBeSaved = $object->isNewRecord;
		
		foreach($information->getInformationDatas() as $fieldName => $fieldValue)
		{
			$this->checkAttribute($object, $fieldName);
			if($object->getAttribute($fieldName) !== $fieldValue)
			{
				$object->setAttribute($fieldName, $fieldValue);
				$needsToBeSaved = true;
			}
		}
		
		foreach($information->getInformationRelations() as $fieldName => $fieldValue)
		{
			$targetModel = $this->getModel($this->findClassNameForRelation($object, $fieldName));
			$tpk = $this->getPrimaryKey($targetModel);
			/** @var ?\yii\db\ActiveRecord $targetObject */
			$targetObject = $targetModel::findOne([$tpk => \sha1($fieldValue)]);
			if(null === $targetObject)
			{
				$message = 'Failed to find target object {target} from relation {class}:{relation} with id {pk}:{id}';
				$context = [
					'target' => \get_class($targetModel),
					'class' => \get_class($object),
					'relation' => $fieldName,
					'pk' => $tpk,
					'id' => \sha1($fieldValue),
				];
				
				throw new Exception(BaseYii::t('InformationModule.InformationPathResolver', $message, $context));
			}
			
			// assume relations are given from the pov of the multi object
			$relation = $this->getRelation($object, $fieldName);
			
			foreach($relation->link as $fk => $pk)
			{
				$this->checkAttribute($object, (string) $pk);
				$this->checkAttribute($targetObject, (string) $fk);
				if($object->getAttribute((string) $pk) !== $targetObject->getAttribute((string) $fk))
				{
					$needsToBeSaved = true;
					$object->setAttribute((string) $pk, $targetObject->getAttribute((string) $fk));
				}
			}
		}
		
		if($needsToBeSaved)
		{
			$this->saveModel($object);
		}
		
		return true;
	}
	
}
