<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Iterator;
use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitor;
use PhpExtended\Information\InformationVisitorInterface;

/**
 * InformationConfirmationHandler class file.
 *
 * This class represents a handler which has two handlers. The first handler
 * is to confirm that the information must be processed by the second handler.
 *
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class InformationConfirmationHandler extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The information confirmator.
	 *
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_confirmator;
	
	/**
	 * The information handler.
	 *
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_handler;
	
	/**
	 * Builds a new ConfirmationHandler with the given confirmator and the given
	 * handler.
	 * The confirmator is a first handler which confirms that the
	 * inforamtion should effectively be processed by the handler.
	 *
	 * @param InformationVisitorInterface<boolean> $confirmator
	 * @param InformationVisitorInterface<boolean> $handler
	 */
	public function __construct(InformationVisitorInterface $confirmator, InformationVisitorInterface $handler)
	{
		$this->_confirmator = $confirmator;
		$this->_handler = $handler;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 */
	public function visitIterator(Iterator $informationIterator)
	{
		$lres = true;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information) && $lres;
		}
		
		return $lres;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		if(!$information->beVisitedBy($this->_confirmator))
		{
			return (bool) $information->beVisitedBy($this->_handler);
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		if(!$information->beVisitedBy($this->_confirmator))
		{
			return (bool) $information->beVisitedBy($this->_handler);
		}
		
		return false;
	}
	
}
