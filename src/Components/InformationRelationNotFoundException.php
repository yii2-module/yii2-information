<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Throwable;
use yii\base\Exception;
use yii\BaseYii;

/**
 * InformationRelationNotFoundException class file.
 *
 * This class represents a relation that is not found in an active record.
 *
 * @author Anastaszor
 */
class InformationRelationNotFoundException extends Exception
{
	
	/**
	 * The class name of the model that does not have the relation.
	 *
	 * @var string
	 */
	protected string $_modelClassName;
	
	/**
	 * The name of the relation not found in the model.
	 *
	 * @var string
	 */
	protected string $_relationName;
	
	/**
	 * Builds a new InformationRelationNotFoundException with the given model
	 * and wanted relation.
	 *
	 * @param string $modelClassName
	 * @param string $relationName
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(string $modelClassName, string $relationName, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_modelClassName = $modelClassName;
		$this->_relationName = $relationName;
		if(null === $message || '' === $message)
		{
			$message = BaseYii::t('InformationModule.InformationRelationNotFoundException', 'Failed to find relation {name} in class {class}', [
				'name' => $relationName,
				'class' => $modelClassName,
			]);
		}
		parent::__construct($message, (int) $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Exception::getName()
	 */
	public function getName() : string
	{
		return __CLASS__;
	}
	
	/**
	 * Gets the class name of the model that does not have the wanted relation.
	 *
	 * @return string
	 */
	public function getModelClassName() : string
	{
		return $this->_modelClassName;
	}
	
	/**
	 * Gets the relation not found in the model.
	 *
	 * @return string
	 */
	public function getRelationName() : string
	{
		return $this->_relationName;
	}
	
}
