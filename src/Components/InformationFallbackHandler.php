<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Iterator;
use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitor;
use PhpExtended\Information\InformationVisitorInterface;

/**
 * InformationFallbackHandler class file.
 * 
 * This class represents a visitor that tries to process an information with
 * its first handler, then falls back on the fallback handler if the first
 * handler fails.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class InformationFallbackHandler extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The primary visitor to handle exceptions from.
	 * 
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_primary;
	
	/**
	 * The fallback visitor to handle informations that make exceptions.
	 * 
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_fallback;
	
	/**
	 * Builds a new InformationFallbackHandler with its primary and fallback
	 * handlers.
	 * 
	 * @param InformationVisitorInterface<boolean> $primary
	 * @param InformationVisitorInterface<boolean> $fallback
	 */
	public function __construct(InformationVisitorInterface $primary, InformationVisitorInterface $fallback)
	{
		$this->_primary = $primary;
		$this->_fallback = $fallback;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 */
	public function visitIterator(Iterator $informationIterator)
	{
		$lres = true;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information) && $lres;
		}
		
		return $lres;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		try
		{
			$success = $this->_primary->visitTriple($information);
		}
		catch(InformationAttributeNotFoundException $e1)
		{
			$success = false;
		}
		catch(InformationClassNotFoundException $e2)
		{
			$success = false;
		}
		catch(InformationRelationNotFoundException $e3)
		{
			$success = false;
		}
		catch(\yii\db\Exception $e4)
		{
			$success = false;
		}
		
		if($success)
		{
			return true;
		}
		
		return (bool) $this->_fallback->visitTriple($information);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		try
		{
			$success = $this->_primary->visitObject($information);
		}
		catch(InformationAttributeNotFoundException $e1)
		{
			$success = false;
		}
		catch(InformationClassNotFoundException $e2)
		{
			$success = false;
		}
		catch(InformationRelationNotFoundException $e3)
		{
			$success = false;
		}
		catch(\yii\db\Exception $e4)
		{
			$success = false;
		}
		
		if($success)
		{
			return true;
		}
		
		return (bool) $this->_fallback->visitObject($information);
	}
	
	
}
