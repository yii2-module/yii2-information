<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationStateInformation;
use PhpExtended\Information\InformationVisitorInterface;
use Yii2Module\Yii2Information\Models\InformationObject;

class InformationObjectAdapter implements InformationObjectInterface
{
	
	/**
	 * The InformationObject record.
	 * 
	 * @var InformationObject
	 */
	protected InformationObject $_record;
	
	/**
	 * Builds a new InformationObjectAdapter with the given record.
	 * 
	 * @param InformationObject $record
	 */
	public function __construct(InformationObject $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.((string) $this->_record->information_object_id);
	}
	
	/**
	 * Gets whether the field name should not be hashed.
	 *
	 * @param string $fieldName
	 * @return boolean
	 */
	public function isRawField(string $fieldName) : bool
	{
		foreach([
			// boolean fields
			'boolean_', 'bool_', 'is_', 'has_',
			// integer fields
			'integer_', 'int_', 'nb_', 'qty_',
			// float fields
			'float_', 'double_',
			// date fields
			'datetime_', 'time_', 'date_',
		] as $prefix)
		{
			if(\str_starts_with($fieldName, $prefix))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getState()
	 */
	public function getState() : InformationStateInformation
	{
		return new InformationState(false, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_record->info_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getCreationDate()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getCreationDate() : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->_record->meta_created_date);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getSupportClass()
	 */
	public function getSupportClass() : string
	{
		return $this->_record->object_class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKey()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function getPrimaryKey() : array
	{
		/** @psalm-suppress MixedReturnTypeCoercion */
		return (array) \json_decode($this->_record->object_keys, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKeyMd5()
	 */
	public function getPrimaryKeyMd5() : array
	{
		$nkeys = [];
		
		foreach($this->getPrimaryKey() as $fieldName => $value)
		{
			$nkeys[$fieldName] = \md5($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getSmartPrimaryKeyMd5()
	 */
	public function getSmartPrimaryKeyMd5() : array
	{
		$nkeys = [];
		
		foreach($this->getPrimaryKey() as $fieldName => $value)
		{
			$nkeys[$fieldName] = $this->isRawField($fieldName) ? $value : \md5($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKeySha1()
	 */
	public function getPrimaryKeySha1() : array
	{
		$nkeys = [];
		
		foreach($this->getPrimaryKey() as $fieldName => $value)
		{
			$nkeys[$fieldName] = \sha1($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getSmartPrimaryKeySha1()
	 */
	public function getSmartPrimaryKeySha1() : array
	{
		$nkeys = [];
		
		foreach($this->getPrimaryKey() as $fieldName => $value)
		{
			$nkeys[$fieldName] = $this->isRawField($fieldName) ? $value : \sha1($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationDatas()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function getInformationDatas() : array
	{
		/** @psalm-suppress MixedReturnTypeCoercion */
		return (array) \json_decode($this->_record->object_data, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationData()
	 */
	public function getInformationData(?string $name) : ?string
	{
		$datas = $this->getInformationDatas();
		if(isset($datas[(string) $name]))
		{
			return (string) $datas[(string) $name];
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::removeInformationData()
	 */
	public function removeInformationData(?string $name) : bool
	{
		$datas = $this->getInformationDatas();
		$res = isset($datas[(string) $name]);
		unset($datas[(string) $name]);
		$this->_record->object_data = (string) \json_encode($datas, \JSON_HEX_TAG | \JSON_HEX_APOS | \JSON_HEX_QUOT | \JSON_HEX_AMP);
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationRelations()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function getInformationRelations() : array
	{
		/** @psalm-suppress MixedReturnTypeCoercion */
		return (array) \json_decode($this->_record->object_rels, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationRelation()
	 */
	public function getInformationRelation(?string $name) : ?string
	{
		$relations = $this->getInformationRelations();
		if(isset($relations[(string) $name]))
		{
			return (string) $relations[(string) $name];
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::removeInformationRelation()
	 */
	public function removeInformationRelation(?string $name) : bool
	{
		$relations = $this->getInformationRelations();
		$res = isset($relations[(string) $name]);
		unset($relations[(string) $name]);
		$this->_record->object_rels = (string) \json_encode($relations, \JSON_HEX_TAG | \JSON_HEX_APOS | \JSON_HEX_QUOT | \JSON_HEX_AMP);
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::isCacheable()
	 */
	public function isCacheable() : bool
	{
		foreach(\array_keys($this->getPrimaryKey()) as $fieldName)
		{
			// date prefix in primary keys implies historisation
			foreach(['datetime_', 'time_', 'date_'] as $prefix)
			{
				if(\mb_strpos($fieldName, $prefix) === 0)
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getEtag()
	 */
	public function getEtag() : string
	{
		$key = $this->getPrimaryKey();
		\ksort($key);
		$spk = (string) \json_encode($key);
		$data = $this->getInformationDatas();
		\ksort($data);
		$sdata = (string) \json_encode($data);
		$rel = $this->getInformationRelations();
		\ksort($rel);
		$srel = (string) \json_encode($rel);
		
		return \sha1($spk.'|'.$sdata.'|'.$srel);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::beVisitedBy()
	 */
	public function beVisitedBy(InformationVisitorInterface $visitor) : bool
	{
		return (bool) $visitor->visitObject($this);
	}
	
}
