<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use PhpExtended\Information\InformationVisitorInterface;
use Throwable;
use yii\base\Module;
use yii\BaseYii;
use yii\log\Logger;
use Yii2Module\Yii2Information\Models\InformationObject;
use Yii2Module\Yii2Information\Models\InformationTriple;

/**
 * InformationBatchProcessor class file.
 *
 * This class processes all the informations that are stored into the databases
 * and gives them to the inner processor or resolver. Once the information is
 * processed successfully, this processor deletes the information in the database.
 *
 * @author Anastaszor
 * @template T
 */
class InformationBatchProcessor
{
	
	public const CRITERIA_LIMIT = 5000;
	
	/**
	 * The inner processor, or resolver.
	 *
	 * @var InformationVisitorInterface<T>
	 */
	protected InformationVisitorInterface $_resolver;
	
	/**
	 * Builds a new InformationBatchProcessor with the given visitor as inner
	 * processor.
	 *
	 * @param InformationVisitorInterface<T> $resolver
	 */
	public function __construct(InformationVisitorInterface $resolver)
	{
		$this->_resolver = $resolver;
	}
	
	/**
	 * Processes all the tables for given module.
	 *
	 * @param Module $module
	 */
	public function processForModule(Module $module) : void
	{
		$this->processInformationObject($module);
		$this->processInformationTriple($module);
	}
	
	/**
	 * Processes the table information triple for given module.
	 * 
	 * @param Module $module
	 */
	public function processInformationTriple(Module $module) : void
	{
		$limit = self::CRITERIA_LIMIT;
		$count = InformationTriple::find()
			->where(['module_id' => $module->id])
			->count()
		;
		
		while(0 < $count)
		{
			$data = InformationTriple::find()
				->where(['module_id' => $module->id])
				->orderBy(['information_triple_id' => \SORT_ASC])
				->limit($limit)
				->all()
			;
			
			/** @var InformationTriple $record */
			foreach($data as $record)
			{
				try
				{
					$information = new InformationTripleAdapter($record);
					$information->beVisitedBy($this->_resolver);
					$record->delete();
				}
				catch(Throwable $exc)
				{
					BaseYii::getLogger()->log($exc->getMessage()."\n\n".$exc->getTraceAsString(), Logger::LEVEL_ERROR, 'InformationModule.InformationBatchProcessor.'.$module->id);
				}
			}
			
			$count = InformationTriple::find()
				->where(['module_id' => $module->id])
				->count()
			;
		}
	}
	
	/**
	 * Processes the table information object for given module.
	 * 
	 * @param Module $module
	 */
	public function processInformationObject(Module $module) : void
	{
		$limit = self::CRITERIA_LIMIT;
		$count = InformationObject::find()
			->where(['module_id' => $module->id])
			->count()
		;
		
		while(0 < $count)
		{
			$data = InformationObject::find()
				->where(['module_id' => $module->id])
				->orderBy(['information_object_id' => $module->id])
				->limit($limit)
				->all()
			;
			
			/** @var InformationObject $record */
			foreach($data as $record)
			{
				try
				{
					$information = new InformationObjectAdapter($record);
					$information->beVisitedBy($this->_resolver);
					$record->delete();
				}
				catch(Throwable $exc)
				{
					BaseYii::getLogger()->log($exc->getMessage()."\n\n".$exc->getTraceAsString(), Logger::LEVEL_ERROR, 'InformationModule.InformationBatchProcessor.'.$module->id);
				}
			}
			
			$count = InformationObject::find()
				->where(['module_id' => $module->id])
				->count()
			;
		}
	}
	
}
