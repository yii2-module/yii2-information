<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Iterator;
use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitor;
use PhpExtended\Information\InformationVisitorInterface;
use yii\base\Exception;
use yii\base\Module;
use yii\BaseYii;
use Yii2Module\Yii2Information\Models\InformationObject;
use Yii2Module\Yii2Information\Models\InformationTriple;

/**
 * InformationRecordHandler class file.
 *
 * This class is made to accept all incoming informations and stores them
 * within this module's tables.
 *
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class InformationRecordHandler extends InformationVisitor implements InformationVisitorInterface
{
	
	public const ERRCODE = 1001;
	
	/**
	 * The module for which to record the given informations.
	 *
	 * @var Module
	 */
	protected Module $_module;
	
	/**
	 * Builds a new InformationRecordHandler for given module.
	 *
	 * @param Module $module
	 */
	public function __construct(Module $module)
	{
		$this->_module = $module;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->_module->getUniqueId();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 */
	public function visitIterator(Iterator $informationIterator) : bool
	{
		$res = true;
		
		foreach($informationIterator as $information)
		{
			$res = $this->visitInformation($information) && $res;
		}
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		if(empty($information->getSubject()) || empty($information->getPredicate()))
		{
			return false;
		}
		
		$record = new InformationTriple();
		$record->module_id = (string) $this->_module->getUniqueId();
		$record->info_id = (string) $information->getId();
		$record->subject = (string) $information->getSubject();
		$record->predicate = (string) $information->getPredicate();
		$record->object = (string) $information->getObject();
		
		$exc = null;
		
		try
		{
			if($record->save())
			{
				return true;
			}
			
			$errors = [];
			
			foreach((array) $record->getErrorSummary(true) as $err)
			{
				$errors[] = (string) $err;
			}
			
			$error = \implode(', ', $errors);
		}
		catch(Exception $exc)
		{
			$error = $exc->getMessage();
		}
		
		$message = "Failed to save information triple :
	module_id : {module}
	info_id   : {infoid}
	subject   : {subject}
	predicate : {predicate}
	object    : {object} (length : {len})
with the given error message : {errmsg}\n\n";
		
		$context = [
			'module' => $record->module_id,
			'infoid' => $record->info_id,
			'subject' => $record->subject,
			'predicate' => $record->predicate,
			'object' => (\mb_strlen($record->object) > 120 ? ((string) \mb_substr($record->object, 0, 117)).'...' : $record->object),
			'len' => \mb_strlen($record->object),
			'errmsg' => $error,
		];
		
		throw new Exception(BaseYii::t('InformationModule.InformationHandler', $message, $context), self::ERRCODE, $exc);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		if(empty($information->getPrimaryKey()))
		{
			return false;
		}
		
		$record = new InformationObject();
		$record->module_id = (string) $this->_module->getUniqueId();
		$record->info_id = (string) $information->getId();
		$record->object_class = (string) $information->getSupportClass();
		$record->object_keys = (string) \json_encode($information->getPrimaryKey(), \JSON_HEX_TAG | \JSON_HEX_APOS | \JSON_HEX_QUOT | \JSON_HEX_AMP);
		$record->object_data = (string) \json_encode($information->getInformationDatas(), \JSON_HEX_TAG | \JSON_HEX_APOS | \JSON_HEX_QUOT | \JSON_HEX_AMP);
		$record->object_rels = (string) \json_encode($information->getInformationRelations(), \JSON_HEX_TAG | \JSON_HEX_APOS | \JSON_HEX_QUOT | \JSON_HEX_AMP);
		if(empty($record->object_data))
		{
			$record->object_data = '[]';
		}
		if(empty($record->object_rels))
		{
			$record->object_rels = '[]';
		}
		
		$exc = null;
		
		try
		{
			if($record->save())
			{
				return true;
			}
			
			$errors = [];
			
			foreach((array) $record->getErrorSummary(true) as $err)
			{
				$errors[] = (string) $err;
			}
			
			$error = \implode(', ', $errors);
		}
		catch(Exception $exc)
		{
			$error = $exc->getMessage();
		}
		
		$message = "Failed to save information object:
	module_id        : {module}
	info_id          : {infoid}
	object_class     : {class}
	object_keys      : {keys}
	object_relations : {relations}
	object_data      : {data}
with the given error message : {errmsg}\n\n";
		
		$context = [
			'module' => $record->module_id,
			'infoid' => $record->info_id,
			'class' => $record->object_class,
			'keys' => $record->object_keys,
			'relations' => $record->object_rels,
			'data' => $record->object_data,
			'errmsg' => $error,
		];
		
		throw new Exception(BaseYii::t('InformationModule.InformationHandler', $message, $context), self::ERRCODE, $exc);
	}
	
}
