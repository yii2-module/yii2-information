<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Throwable;
use yii\base\Exception;
use yii\BaseYii;

/**
 * InformationClassNotFoundException class file.
 *
 * This class represents a class that is not found and could not be autoloaded.
 *
 * @author Anastaszor
 */
class InformationClassNotFoundException extends Exception
{
	
	/**
	 * The class that does not exists.
	 *
	 * @var string
	 */
	protected string $_modelClassName;
	
	/**
	 * Builds a new InformationClassNotFoundException with the given classname.
	 *
	 * @param string $modelClassName
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(string $modelClassName, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_modelClassName = $modelClassName;
		if(null === $message || '' === $message)
		{
			$message = BaseYii::t('InformationModule.InformationClassNotFoundException', 'Failed to load class {class}', [
				'class' => $modelClassName,
			]);
		}
		parent::__construct($message, (int) $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Exception::getName()
	 */
	public function getName() : string
	{
		return __CLASS__;
	}
	
	/**
	 * Gets the class name of the model that could not be autoloaded.
	 *
	 * @return string
	 */
	public function getModelClassName() : string
	{
		return $this->_modelClassName;
	}
	
}
