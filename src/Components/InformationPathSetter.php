<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;
use yii\BaseYii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * InformationPathSetter class file.
 *
 * This class is a resolver (meaning it will register the information not in
 * the information table, but will try to resolve it to save it at the right
 * place in the target relational model) that uses the "path" field as foreign
 * primary key. It supposes that all the class names that are stored into the
 * support class field of the informations have that "path" field if they are
 * not composite. (it does not matter if they are).
 *
 * All the methods return a boolean which is true if the information was
 * successfully processed and an information was saved, and false if the
 * information could not be processed. An exception is thrown in case the
 * metadata of the information are comparatively to the model state and the
 * information cannot be considered as processed and removed.
 *
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 */
class InformationPathSetter extends InformationResolver implements InformationVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		// do not check triple objects, they do not have paths
		return !(empty($information->getSubject()) || empty($information->getPredicate()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitMulti()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		if(empty($information->getPrimaryKey()))
		{
			return false;
		}
		
		$model = $this->getSupportModel($information);
		$this->checkAttribute($model, 'path');
		$object = $model::findOne(['path' => \implode('|', $information->getPrimaryKey())]);
		if(null === $object)
		{
			$object = clone $model;
			$object->setIsNewRecord(true);
			$object->setAttribute('path', (string) \implode('|', $information->getPrimaryKey()));
		}
		$needsToBeSaved = $object->isNewRecord;
		
		foreach($information->getInformationDatas() as $fieldName => $fieldValue)
		{
			$this->checkAttribute($object, $fieldName);
			if($object->getAttribute($fieldName) !== $fieldValue)
			{
				$object->setAttribute($fieldName, $fieldValue);
				$needsToBeSaved = true;
			}
		}
		
		foreach($information->getInformationRelations() as $fieldName => $fieldValue)
		{
			$targetModel = $this->getModel($this->findClassNameForRelation($object, $fieldName));
			$this->checkAttribute($targetModel, 'path');
			/** @var ?ActiveRecord $targetObject */
			$targetObject = $targetModel::findOne([
				'path' => $fieldValue,
			]);
			if(null === $targetObject)
			{
				$message = 'Failed to find target object {target} from relation {class}:{relation} with id {id}';
				$context = [
					'target' => \get_class($targetModel),
					'class' => \get_class($object),
					'relation' => $fieldName,
					'id' => $fieldValue,
				];
				
				throw new Exception(BaseYii::t('InformationModule.InformationPathResolver', $message, $context));
			}
			
			// assume relations are given from the pov of the multi object
			$relation = $this->getRelation($object, $fieldName);
			
			foreach($relation->link as $fk => $pk)
			{
				$this->checkAttribute($object, (string) $pk);
				$this->checkAttribute($targetObject, (string) $fk);
				if($object->getAttribute((string) $pk) !== $targetObject->getAttribute((string) $fk))
				{
					$object->setAttribute((string) $pk, $targetObject->getAttribute((string) $fk));
					$needsToBeSaved = true;
				}
			}
		}
		
		if($needsToBeSaved)
		{
			$this->saveModel($object);
		}
		
		return true;
	}
	
}
