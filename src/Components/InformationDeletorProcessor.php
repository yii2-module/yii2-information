<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use yii\base\Module;
use Yii2Module\Yii2Information\Models\InformationObject;
use Yii2Module\Yii2Information\Models\InformationTriple;

/**
 * InformationDeletorProcessor class file.
 *
 * This file process all records for a given module by deleting all rows about
 * that module.
 *
 * @author Anastaszor
 */
class InformationDeletorProcessor
{
	
	/**
	 * Process the deletion for all records in all tables for given module.
	 *
	 * @param ?Module $module
	 * @return integer the number of rows deleted
	 */
	public function processForModule(?Module $module) : int
	{
		if(null === $module)
		{
			return 0;
		}
		
		$del = 0;
		$del += $this->processInformationObject($module);
		$del += $this->processInformationTriple($module);
		
		return $del;
	}
	
	/**
	 * Processes the table information triple for given module.
	 * 
	 * @param Module $module
	 * @return integer the number of rows deleted
	 */
	public function processInformationTriple(Module $module) : int
	{
		try
		{
			return InformationTriple::deleteAll([
				'module_id' => $module->id,
			]);
		}
		catch(\yii\base\NotSupportedException $e)
		{
			return 0;
		}
	}
	
	/**
	 * Processes the table information object for given module.
	 * 
	 * @param Module $module
	 * @return integer the number of rows deleted
	 */
	public function processInformationObject(Module $module) : int
	{
		try
		{
			return InformationObject::deleteAll([
				'module_id' => $module->id,
			]);
		}
		catch(\yii\base\NotSupportedException $e)
		{
			return 0;
		}
	}
	
}
