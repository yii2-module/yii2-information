<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;

/**
 * InformationPathChecker class file.
 *
 * This class is a resolver (meaning it will try to resolve the information to
 * check if it exists in the right tables on the relational model) that uses
 * the "path" field as foreign primary key. It supposes that all the class names
 * that are stored into the support class field of the informations have that
 * "path" field if they are not composite. (it does not matter if they are).
 *
 * All the methods return a boolean which is true if the information already
 * exists and is the same in the model state, and false if the information does
 * not exists or exists and is not the same. If the metadata is incorrect, no
 * exceptions are thrown, the method will return false as if the information
 * does not exists in current model state.
 *
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 */
class InformationPathChecker extends InformationResolver implements InformationVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 */
	public function visitTriple(InformationTripleInterface $information) : bool
	{
		if(empty($information->getSubject()) || empty($information->getPredicate()))
		{
			return false;
		}
		
		$supportModel = $this->getSupportModel($information);
		
		$this->checkAttribute($supportModel, 'subject');
		$this->checkAttribute($supportModel, 'predicate');
		$this->checkAttribute($supportModel, 'object');
		
		$supportObject = $supportModel::findOne([
			'subject' => $information->getSubject(),
			'predicate' => $information->getPredicate(),
			'object' => $information->getObject(),
		]);
		
		return null !== $supportObject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 */
	public function visitObject(InformationObjectInterface $information) : bool
	{
		if(empty($information->getPrimaryKey()))
		{
			return false;
		}
		
		$supportModel = $this->getSupportModel($information);
		
		// phase 1 : get a record
		$this->checkAttribute($supportModel, 'path');
		
		/** @var ?\yii\db\ActiveRecord $object */
		$object = $supportModel::findOne([
			'path' => \implode('|', $information->getPrimaryKey()),
		]);
		if(null === $object)
		{
			return false;
		}
		
		// phase 2 : check all the data
		
		foreach($information->getInformationDatas() as $fieldName => $fieldValue)
		{
			$this->checkAttribute($supportModel, $fieldName);
			
			if((string) $object->getAttribute($fieldName) !== (string) $fieldValue)
			{
				return false;
			}
		}
		
		// phase 3 : check all the relations
		
		foreach($information->getInformationRelations() as $fieldName => $fieldValue)
		{
			// we are systematically in a n<-1 relation
			$targetModel = $this->getModel($this->findClassNameForRelation($object, $fieldName));
			$targetObject = $targetModel::findOne([
				'path' => $fieldValue,
			]);
			if(null === $targetObject)
			{
				return false;
			}
			
			$relation = $this->getRelation($supportModel, $fieldName);
			
			foreach($relation->link as $fk => $pk)
			{
				$this->checkAttribute($supportModel, (string) $pk);
				$this->checkAttribute($targetObject, (string) $fk);
				if((string) $supportModel->getAttribute((string) $pk) !== (string) $targetObject->getAttribute((string) $fk))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
}
