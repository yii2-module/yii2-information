<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Exception;
use Iterator;
use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationVisitor;
use Stringable;
use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * InformationResolver class file.
 * 
 * This class is a generic method provider for all information handlers and
 * resolvers.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
abstract class InformationResolver extends InformationVisitor implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitIterator()
	 * @extends \Iterator<InformationInterface> $informationIterator
	 */
	public function visitIterator(Iterator $informationIterator)
	{
		$lres = true;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information) && $lres;
		}
		
		return $lres;
	}
	
	/**
	 * Saves the given model record.
	 *
	 * @param ActiveRecord $object
	 * @param array<string, string> $attributeNames
	 * @throws Exception
	 */
	protected function saveModel(ActiveRecord $object, array $attributeNames = []) : void
	{
		if($object->save(true, $attributeNames))
		{
			return;
		}
		
		$errlist = [];
		
		foreach($object->getErrors() as $attrname => $errorlist)
		{
			$value = $object->{$attrname};
			$type = \gettype($value);
			/** @psalm-suppress MixedArgument */
			$strval = $this->getStrval($value);
			$newErrList = [];
			
			foreach($errorlist as $key => $value)
			{
				$newErrList[(string) $key] = (string) $value;
			}
			
			$errlist[] = ((string) $attrname).' ('.$type.') with value ('.$strval.') : '.\implode(' ; ', $newErrList);
		}
		
		$message = "Failed to save {record} : \n{errors}";
		$context = [
			'record' => \get_class($object),
			'errors' => \implode("\n", $errlist),
		];
		
		throw new Exception(BaseYii::t('InformationModule.InformationPathSetter', $message, $context));
	}
	
	/**
	 * Gets a string type of the given value.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string> $value
	 * @return string
	 */
	protected function getStrval($value) : string
	{
		$type = \gettype($value);
		
		switch ($type)
		{
			case 'NULL':
				return 'null';
				
			case 'boolean':
				return $value ? 'true' : 'false';
				
			case 'integer':
			case 'double':
			case 'string':
				/** @phpstan-ignore-next-line */
				return \mb_strlen((string) $value) > 30 ? ((string) \mb_substr((string) $value, 0, 27)).'...' : (string) $value;
				
			default:
				return 'not scalar';
		}
	}
	
	/**
	 * Gets the primary key of the given record.
	 *
	 * @param ActiveRecord $record
	 * @return string
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function getPrimaryKey(ActiveRecord $record) : string
	{
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		return (string) $record->getTableSchema()->primaryKey[0];
	}
	
	/**
	 * Gets the schema that is given by this information.
	 *
	 * @param InformationInterface $information
	 * @return ActiveRecord
	 * @throws InformationClassNotFoundException if the support class does not
	 *                                           exists or is not autoloadable
	 */
	protected function getSupportModel(InformationInterface $information)
	{
		return $this->getModel($information->getSupportClass());
	}
	
	/**
	 * Gets the name of the class that is at the end of the relation with given
	 * relation name for given active record model.
	 *
	 * @param ActiveRecord $model
	 * @param string $relationName
	 * @return class-string
	 * @throws InformationRelationNotFoundException if the relation with given
	 *                                              name does not exists in the model class
	 * @psalm-suppress InvalidReturnType
	 */
	protected function findClassNameForRelation(ActiveRecord $model, $relationName) : string
	{
		if('@self' === $relationName)
		{
			return \get_class($model);
		}
		
		/** @var ActiveQuery $relation */
		$relation = $this->getRelation($model, $relationName);
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $relation->modelClass;
	}
	
	/**
	 * Gets the relation with given relation name from the given active record
	 * model.
	 *
	 * @param ActiveRecord $model
	 * @param string $relationName
	 * @return ActiveQuery
	 * @throws InformationRelationNotFoundException if the relation with given
	 *                                              name does not exists in the model class
	 */
	protected function getRelation(ActiveRecord $model, $relationName)
	{
		try
		{
			$res = $model->getRelation($relationName);
			if($res instanceof ActiveQuery)
			{
				return $res;
			}
		}
		catch(\yii\base\InvalidArgumentException $e)
		{
			throw new InformationRelationNotFoundException(\get_class($model), $relationName, null, -1, $e);
		}
		
		throw new InformationRelationNotFoundException(\get_class($model), $relationName, null, -1);
	}
	
	/**
	 * Gets the active record model for given class name.
	 *
	 * @param string $className
	 * @return ActiveRecord
	 * @throws InformationClassNotFoundException if the class does not exists
	 *                                           or is not loadable or is not an ActiveRecord subclass
	 */
	protected function getModel($className)
	{
		if(\class_exists($className) && \is_subclass_of($className, ActiveRecord::class))
		{
			return $className::instance();
		}
		
		throw new InformationClassNotFoundException($className);
	}
	
	/**
	 * Checks whether the given record model has the given attribute field.
	 *
	 * @param ?ActiveRecord $model
	 * @param string $attributeName
	 * @throws \yii\base\InvalidArgumentException if multiple field names are given
	 * @throws InformationAttributeNotFoundException if the attribute does
	 *                                               not exists in the given model
	 */
	protected function checkAttribute(?ActiveRecord $model, string $attributeName) : void
	{
		if(null === $model)
		{
			throw new InformationAttributeNotFoundException('null', $attributeName);
		}
		
		if(!$model->hasAttribute($attributeName))
		{
			throw new InformationAttributeNotFoundException(\get_class($model), $attributeName);
		}
	}
	
}
