<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-information library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Information\Components;

use Throwable;
use yii\base\Exception;
use yii\BaseYii;

/**
 * InformationAttributeNotFoundException class file.
 *
 * This class represents an attribute that is not found in a model.
 *
 * @author Anastaszor
 */
class InformationAttributeNotFoundException extends Exception
{
	
	/**
	 * The class name of the model that does not have the attribute.
	 *
	 * @var string
	 */
	protected string $_modelClassName;
	
	/**
	 * The name of the attribute not found in the model.
	 *
	 * @var string
	 */
	protected string $_attributeName;
	
	/**
	 * Builds a new InformationAttributeNotFoundException with the given model
	 * and wanted attribute.
	 *
	 * @param string $modelClassName
	 * @param string $attributeName
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(string $modelClassName, string $attributeName, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_modelClassName = $modelClassName;
		$this->_attributeName = $attributeName;
		if(null === $message || '' === $message)
		{
			$message = BaseYii::t('InformationModule.InformationAttributeNotFoundException', 'Failed to find field {field} in class {class}.', [
				'field' => $attributeName,
				'class' => $modelClassName,
			]);
		}
		parent::__construct($message, (int) $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Exception::getName()
	 * @return string
	 */
	public function getName() : string
	{
		return __CLASS__;
	}
	
	/**
	 * Gets the class name of the model that does not have the wanted attribute.
	 *
	 * @return string
	 */
	public function getModelClassName() : string
	{
		return $this->_modelClassName;
	}
	
	/**
	 * Gets the attribute name not found in the model.
	 *
	 * @return string
	 */
	public function getAttributeName() : string
	{
		return $this->_attributeName;
	}
	
}
